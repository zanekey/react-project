HOMY'S CAR SHARING V4.0
Release note:
    - All main functions of application is completed and finalized
    - Fixed some bugs

Installation and running instruction:
    -  Install Node.js
    -  To install dependencies run commands below:
        npm install
        npm run client-install
    - Run command below to start application:
        npm run start-project
Please refer to "Development guide" section in Project Report for indept walkthroughs

Deployment URL:
https://damp-wildwood-73011.herokuapp.com


